<?php

/**
 * @file
 * Primary module hooks for Earthdata Utilities module.
 *
 * @DCG
 * This file is no longer required in Drupal 8.
 * @see https://www.drupal.org/node/2217931
 */

use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Routing\RouteMatchInterface;
// use Drupal\node\Entity\Node;
// use Drupal\Core\Datetime\DrupalDateTime;
// use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Implements hook_help().
 */
function earthdata_utilities_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.earthdata_utilities':
      $output = '<h3>' . t('Earthdata Utilities Module') . '</h3>';
      $output .= '<p><strong>' . t('Some helpful clean-up utilities (proceed at your own risk):') . '</strong></p>';
      $output .= '<p>' . t('<a href=":link1">Clean URL Redirects</a>', [':link1' => '/earthdata-utilities/clean-url-redirects']) . '</p>';
      $output .= '<ul><li>' . t('Removes redirects which have matching URL aliases, which result in 500 errors.') . '</li></ul>';
      $output .= '<p>' . t('<a href=":link2">Fix Missing Files</a>', [':link2' => '/earthdata-utilities/fix-files']) . '</p>';
      $output .= '<ul><li>' . t('Fetches missing files from Conduit. Requires an update to the <code>web/modules/custom/earthdata_utilities/json/files-not-found.json</code> file derived from the Link Checker contrib module and a custom Drupal View (<code>admin/structure/views/view/broken_links_report/edit/data_export_1</code>).') . '</li></ul>';
      $output .= '<p>' . t('<a href=":link3">Fix "Mismatched entity and/or field definitions"</a>', [':link3' => '/earthdata-utilities/update-mismatched-entity-and-or-field-definitions']) . '</p>';
      $output .= '<ul><li>' . t('Reported on the <a href=":link4">/admin/reports/status</a> page.</p>', [':link4' => '/admin/reports/status']) . '</li></ul>';
      $output .= '<p>' . t('<a href=":link5">Remove Absolute Links</a>', [':link5' => '/earthdata-utilities/remove-absolute-links']) . '</p>';
      $output .= '<ul><li>' . t('Replace https://earthdata.nasa.gov with nothing.') . '</p></li></ul>';
      $output .= '<p>' . t('<a href=":link6">Report: Content Delivery Network Assets</a>', [':link6' => '/admin/config/earthdata/remove-content-delivery-network-assets']) . '</p>';
      $output .= '<ul><li>' . t('A report of all pages containing references to CDN-based assets - https://cdn.earthdata.nasa.gov/.') . '</p></li></ul>';
      $output .= '<p>' . t('<a href=":link7">Report: URLs Containing One URL Part</a>', [':link7' => '/admin/config/earthdata/urls-containing-one-url-part']) . '</p>';
      $output .= '<ul><li>' . t('A report of all URLS containing only one URL part (e.g., /hazards-and-disasters, /foo-bar, etc...)') . '</p></li></ul>';
      $output .= '<p>' . t('<a href=":link8">Contact Us Fixes</a>', [':link8' => '/admin/config/earthdata/replace-contact-us']) . '</p>';
      $output .= '<ul><li>' . t('Search and replace Contact Us links (e.g., javascript:feedback.showForm();)') . '</p></li></ul>';
      return $output;
  }
}

/**
 * Implements HOOK_form_alter().
 */
function earthdata_utilities_form_alter(&$form, \Drupal\Core\Form\FormStateInterface $form_state, $form_id) {

  // ESDS-990: Fix Last Updated Dates
  // Override the default value of the timestamp fields to the current date for new records.
  // Target fields: field_date_updated and field_last_reviewed
  // Get all of the node content types.
  $node_types = \Drupal\node\Entity\NodeType::loadMultiple();

  // New record form id example: node_article_form
  // Existing record form id example: node_article_edit_form
  foreach ($node_types as $node_type) {
    if ($form_id === 'node_' . $node_type->id() . '_form') {
      // Get the default timezone.
      $default_timezone = new \DateTimeZone(date_default_timezone_get());
      // Set some date/time objects.
      if (array_key_exists('field_date_updated', $form)) {
        $form['field_date_updated']['widget'][0]['value']['#default_value'] = new \DateTime('now', $default_timezone);
      }
      if (array_key_exists('field_last_reviewed', $form)) {
        $form['field_last_reviewed']['widget'][0]['value']['#default_value'] = new \DateTime('now', $default_timezone);
      }
    }
  }

  // ESDS-447: Add Filter buttons to topic pages
  // ESDS-1024: Filter for related content is broken
  // Remove exposed filter content type options with no results.
  $view_ids = [
    'views-exposed-form-taxonomy-term-page-1',
    'views-exposed-form-taxonomy-term-page-2',
  ];

  if (in_array($form['#id'], $view_ids)) {

    $types = $content_types = [];
    $group_items = $form_state->getStorage();

    if (array_key_exists('filters', $group_items['display']['display_options'])) {

      // Get the view.
      $view = $form_state->get('view');
      // Get the first view argument.
      $term_id = $view->args[0];
      // Load the term by id.
      $term = Term::load($term_id);
      // Set the vocabulary.
      $vocabulary_id = $term->vid->target_id;

      // Get the filter's content types.
      $types = $group_items['display']['display_options']['filters']['type']['group_info']['group_items'];

      // Get targeted content types.
      if (!empty($types)) {
        foreach ($types as $tkey => $tvalue) {
          foreach ($tvalue['value'] as $key => $value) {

            if ($value !== '0') {
              $content_types[$tkey][] = $key;
            }

          }
        }
      }

      // Unset content type options with no results.
      if (!empty($content_types)) {
        foreach ($content_types as $ctkey => $ctypes) {
          // Fetch the nodes.
          $nodes = _get_nodes_by_term_id_and_content_type($term_id, $vocabulary_id, $ctypes);

          if (empty($nodes)) {
            unset($form['type']['#options'][$ctkey]);
          }
        }
      }

    }

    // Show / Hide filter buttons if there is more than the all button and one content type
    if ( array_key_exists('type', $form) && count($form['type']['#options']) <= 2 ) {

      $form['#access'] = false;
      $form['#showtitle'] = false;

    } elseif ( array_key_exists('type', $form) && count($form['type']['#options']) > 2 ) {
      $form['#showtitle'] = true;
    }

  }


  // ESDS-1097: Filters for Data User Profile pages and Data User Chats pages.
  $data_user_view_ids = [
    'views-exposed-form-data-users-block-1',
    'views-exposed-form-data-users-block-2',
    'views-exposed-form-events-block-2'
  ];

  if (in_array($form['#id'], $data_user_view_ids)) {

    if ($form['#id'] === 'views-exposed-form-data-users-block-1') {
      $ctypes = ['data_user_profile'];
    }

    if ($form['#id'] === 'views-exposed-form-data-users-block-2') {
      $ctypes = ['data_chat'];
    }

    if ($form['#id'] === 'views-exposed-form-events-block-2') {
      $ctypes = ['event'];
    }

    // Get the list of Topics.
    $topics = $form['topic']['#options'];

    // Remove the 'All' option.
    //unset($topics['All']);

    // Unset content type options with no results.
    if (!empty($topics)) {
      foreach ($topics as $term_id => $term_name) {

        // Fetch the nodes.
        $nodes = _get_nodes_by_term_id_and_content_type($term_id, 'gcmd', $ctypes);

        if (empty($nodes)) {
          unset($form['topic']['#options'][$term_id]);
        }
      }
    }

    if ( array_key_exists('topic', $form) && count($form['topic']['#options']) <= 2 ) {

      $form['#access'] = FALSE;
      $form['#showtitle'] = FALSE;

    } elseif ( array_key_exists('topic', $form) && count($form['topic']['#options']) > 2 ) {
      $form['#showtitle'] = TRUE;
    }

  }

}

function _get_nodes_by_term_id_and_content_type($term_id, $vocabulary_id, $ctypes) {

  $nodes = $tids = [];

  switch ($vocabulary_id) {
    case 'daac':
      $field = 'field_daacs';
      break;
    case 'gcmd':
      $field = 'field_gcmd_tags';
      break;
    case 'sensors':
      $field = 'field_sensor';
      break;
    case 'technology':
      $field = 'field_technology';
      break;
    case 'campaigns_and_series':
      $field = 'field_campaigns_and_series';
      break;
    case 'program_affiliation':
      $field = 'field_program_affiliation';
      break;
    case 'tags':
      $field = 'field_tags';
      break;
  }

  // Get all of the parent term's child terms.
  $depth = NULL; // 1 to get only immediate children, NULL to load entire tree
  $load_entities = FALSE; // True will return loaded entities rather than ids

  $child_terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')
    ->loadTree($vocabulary_id, $term_id, NULL, $load_entities);

  foreach ($child_terms as $child_term) {
    $tids[] = $child_term->tid;
  }

  // Terms with child terms (e.g. Topics).
  // Loop through child terms to fetch related nodes.
  if (!empty($tids)) {

    foreach ($tids as $tid) {

      $results = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
        $field => $tid,
        'type' => $ctypes,
      ]);

      if (!empty($results)) {
        $nodes[] = $results;
      }

    }

  }

  // Terms without child terms (e.g. DAACs).
  if (empty($tids)) {

    $results = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
      $field => $term_id,
      'type' => $ctypes,
    ]);

    if (!empty($results)) {
      $nodes[] = $results;
    }

  }

  return $nodes;
}

// Views: Add an exposed filter for the Authored on (created) date/time, but only the year.
//
// Resources
// We tried the Views year filter contrib module, no go - https://www.drupal.org/project/views_year_filter.
// Different approach - article found on the interwebs: https://www.flocondetoile.fr/blog/filter-content-year-views-drupal-8

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Views: Add an exposed filter for the Authored on (created) date/time, but only the year.
 *
 * Dev note: The hard-coded version  almost works.
 * With some additional code, the years can be made dynamic if need be.
 */
// function earthdata_utilities_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {
//   if (isset($form['#id']) && $form['#id'] == 'views-exposed-form-collections-block-3') {
//     $options = [
//       'all' => t('- All -'),
//       '2015' => '2015',
//       '2016' => '2016',
//       '2017' => '2017',
//       '2018' => '2018',
//       '2019' => '2019',
//       '2020' => '2020',
//       '2021' => '2021'
//     ];

//     $form['year'] = [
//       '#title' => new TranslatableMarkup('By year'),
//       '#type' => 'select',
//       '#options' => $options,
//       '#size' => NULL,
//       '#default_value' => 'all',
//     ];
//   }
// }

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Views: Add an exposed filter for the Authored on (created) date/time, but only the year.
 *
 * Dev note: This almost works. Be aware there's debugging present - dump(); and dd();
 *
 * Uncomment these class declarations at the top of this file to test:
 * use Drupal\node\Entity\Node;
 * use Drupal\Core\Datetime\DrupalDateTime;
 * use Drupal\Core\Cache\CacheBackendInterface;
 */
// function earthdata_utilities_form_views_exposed_form_alter(&$form, FormStateInterface $form_state, $form_id) {

//   if (isset($form['#id']) && $form['#id'] == 'views-exposed-form-collections-block-3') {

//     $options = &drupal_static(__FUNCTION__);
//     if (is_null($options)) {
//       $cid = 'earthdata_utilities:article:created';
//       // $data = \Drupal::cache()->get($cid);

//       $data = NULL;

//       // dd($data);

//       if (!$data) {

//         // dd('data');

//         $options = [];
//         $options['all'] = new TranslatableMarkup('- All -');
//         $query = \Drupal::entityQuery('node');
//         $query->condition('type', 'article')
//           ->condition('status', 1)
//           ->sort('created', 'ASC');
//         $result = $query->execute();

//         dd($result);


//         if ($result) {
//           $nodes = Node::loadMultiple($result);

//           foreach ($nodes as $node) {
//             $date = $node->created->value;
//             if ($date) {
//               $date = new DrupalDateTime($date, new DateTimeZone('UTC'));
//               $year = $date->format('Y');

//               dd($year);

//               if (!isset($options[$year])) {
//                 $options[$year] = $year;
//               }
//             }
//           }
//         }

//         dd($options);

//         $cache_tags = ['node:article:created'];
//         \Drupal::cache()->set($cid, $options, CacheBackendInterface::CACHE_PERMANENT, $cache_tags);
//       }
//       else {

//         dd('here');

//         $options = $data->data;
//       }

//     }

//     dd($options);

//     $form['year'] = [
//       '#title' => new TranslatableMarkup('By year'),
//       '#type' => 'select',
//       '#options' => $options,
//       '#size' => NULL,
//       '#default_value' => 'All',
//     ];

//   }
// }
