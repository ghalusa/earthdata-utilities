# Earthdata Utilities Module

## Contents of this File

* Introduction
* Installation
* Maintainers

### Introduction

Various utilities for the Earthdata website.

### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Modules](https://www.drupal.org/docs/extending-drupal/installing-modules) for further information.

### Maintainers/Support

* [SSAI](https://www.ssaihq.com/)
* Kathleen Wilson: [kathleen.m.wilson@nasa.gov](kathleen.m.wilson@nasa.gov)
* Scott Brenner: [scott.l.brenner@nasa.gov](scott.l.brenner@nasa.gov)
* Goran Halusa: [goran.n.halusa@nasa.gov](goran.n.halusa@nasa.gov)
