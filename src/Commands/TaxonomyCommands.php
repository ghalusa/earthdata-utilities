<?php
/**
 * Implements drush commands to work with and modify EartData Taxonomy Terms
 * 
 * commands:
 *   ddev drush gensearchurls
 *   ddev drush writetermscsv
 *   ddev drush writematchescsv
 *   ddev drush writecomparisoncsv
 */

namespace Drupal\earthdata_utilities\Commands;

use Drush\Commands\DrushCommands;
use Drupal\media\Entity\Media;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;

class TaxonomyCommands extends DrushCommands {

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /* * * * * * * * *     Generate Search Urls    * * * * * * * * * */
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /**
   * Generates search URL for Earthdata taxonomy terms.
   *
   * @command taxonomy:generate-search-urls
   * @aliases gensearchurls
   *
   * @usage taxonomy:generate-search-urls
   */
  public function generateSearchUrls() {
    if ($this->io()->confirm("This will overwrite the 'Find Data' url for earthdata taxonomy terms that have a matching term on search.earthdata.nasaa.gov.\n  Are you sure you want to do this?")) {
      // ensure taxonomy_matches.csv exists
      if (file_exists("modules/custom/earthdata_utilities/csv_files/taxonomy_matches.csv")) {
        $start = time();

        $this->output()->writeln('Getting term entities');
        // Earthdata taxonomy
        $vid = 'gcmd';
        // get all terms
        $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => $vid]);
        
        $numTerms = count($terms);
        $this->output()->writeln("Found:  $numTerms terms. \n  Updating 'Find Data' urls...");

        // get an array of matching ids from csv file
        $matchesFile = fopen("modules/custom/earthdata_utilities/csv_files/taxonomy_matches.csv", "r");
        $matchIds = [];
        if ($matchesFile !== FALSE) {
          while (($row = fgetcsv($matchesFile, 1000, ",")) !== FALSE) {
            if ($row[0] !== "ID") {
              array_push($matchIds, $row[0]);
            }
          }
        }
        fclose($matchesFile);

        // for testing:
        // $theTerm = Term::load("5966");
        // $theTerm = Term::load("6299");

        // if the term's id is in the array of search/earthdata matches
        // and the field_find_data does not already have values, generate
        // the search url, populate the field, and save.
        $written = 0;
        foreach ($terms as $term) {
          if (in_array($term->id(), $matchIds)) {
          // if ((in_array($term->id(), $matchIds)) && !$term->field_find_data->getValue()) {
            $newUrl = $this->generateTermSearchUrl($term);
            $term->field_find_data->setValue(array('uri' => $newUrl, 'title' => "Find Data"));
            $term->save();
            $written += 1;
          }
        }
    
        $duration = (time()) - $start;
        $this->output()->writeln("gensearchurls complete.\n  $written terms updated in $duration seconds.");
      } else {
        $this->output()->writeln("Error: 'csv_files/taxonomy_matches.csv' not found.\n  gensearchurls cancelled.");
      }
    } else {
      $this->output()->writeln('gensearchurls cancelled.');
    }
  }

  /**
   * Helper function
   * Gets the taxonomy depth of a term
   * 
   * @param Drupal\taxonomy\Entity\Term $term
   *   a taxonomy term
   * @return int $depth
   */
  private function getTaxonomyDepth($term) {
    $ancestors = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($term->id());
    $depth = count($ancestors);
    return $depth;
  }

  /**
   * Helper function
   * Replaces various characters with search url replacements
   * 
   * @param string $text
   * @return string $result
   */
  private function escapeForSearchUrl($text) {
    $find = array(" ", "/", "(", ")");
    $replace = array("+", "%2F", "%28", "%29");
    $result = str_replace($find, $replace, $text);
    return $result;
  }

  /**
   * Helper function
   * Builds a search url for a taxonomy term based on
   * its name and the names of its parents.
   * 
   * https://search.earthdata.nasa.gov/search?
   * level 5: fs30=Cirrus+Cloud+Systems
   * level 3: &fs10=Tropospheric%2FHigh-Level+Clouds+%28Observed%2FAnalyzed%29
   * level 2: &fsm0=Clouds
   * level 4: &fs20=Cirrus%2FSystems
   * level 1: &fst0=Atmosphere
   * 
   * @param Drupal\taxonomy\Entity\Term $term
   *   a taxonomy term
   * @return string $url
   */
  private function generateTermSearchUrl($term) {
    $termId = $term->id();
    $parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($termId);

    $parentsList = [];
    foreach ($parents as $parent) {
      /**
       * creating $parentsList of the format:
       * $parentsList = array( 
       * 1 => [ "id" => $id, "label" => $label ],
       * 2 => [ "id" => $id, "label" => $label ]
       *  ...etc)
       * */
      $id = $parent->id();
      $label = $this->escapeForSearchUrl($parent->label());
      $depth = $this->getTaxonomyDepth($parent);

      $parentsList[$depth] = array(
        "id" => $id,
        "label" => strtolower($label),
      );
    }

    // accounts for difference between "ocean" and "oceans"
    $level1 = ($parentsList[1]["label"] === "ocean") ? "oceans" : $parentsList[1]["label"];
    $level2 = $parentsList[2]["label"];
    $level3 = $parentsList[3]["label"];
    $level4 = $parentsList[4]["label"];
    $level5 = $parentsList[5]["label"];
    // (max depth is currently 5)
    
    // building the url string.  Yes, lvl4 comes after lvl 2.
    $url = "https://search.earthdata.nasa.gov/search?";

    if ($level5) {
      $url .= "fs30=$level5&";
    }
    if ($level3) {
      $url .= "fs10=$level3&";
    }
    if ($level2) {
      $url .= "fsm0=$level2&";
    }
    if ($level4) {
      $url .= "fs20=$level4&";
    }
    if ($level1) {
      $url .= "fst0=$level1";
    }

    return $url;
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /* * * * * * * *    Write ESDS Terms to csv    * * * * * * * * * */
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /**
   * Writes all Earthdata taxonomy terms to a csv file.
   * Resulting rows contain the term id, each parent, the term itself, and the search url.
   *
   * @command taxonomy:write-taxonomy-terms-to-csv
   * @aliases writetermscsv
   *
   * @usage taxonomy:write-taxonomy-terms-to-csv
   */
  public function writeTermsToCsv() {
    if ($this->io()->confirm("This will overwrite earthdata_taxonomy_terms.csv with current term information.  Are you sure you want to do this?")) {
      $start = time();
      $this->output()->writeln('Writing terms to csv');
      $this->output()->writeln('Getting term entities');

      // name of taxonomy I want
      $vid = 'gcmd';
      // get all terms
      $terms = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadByProperties(['vid' => $vid]);
      
      // dd($terms[array_key_first($terms)]);
      $numTerms = count($terms);
      $this->output()->writeln("Found:  $numTerms terms");

      // open a file for writing
      $file = fopen('modules/custom/earthdata_utilities/csv_files/earthdata_taxonomy_terms.csv', 'w');
      $headers = ["ID", "Level 1", "Level 2", "Level 3", "Level 4", "Level 5", "URL"];
      fputcsv($file, $headers);

      //reset $numTerms to 0;
      $numTerms = 0;
      foreach ($terms as $term) {
        
        $termId = $term->id();
        $parents = \Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadAllParents($termId);
        $parentsList = [];
        foreach ($parents as $parent) {
          /**
           * $parentsList = array(
           * "1" => [ "id" => $id, "label" => $label ],
           * "2" => [ "id" => $id, "label" => $label ]
           * ...etc)
           * */
          $id = $parent->id();
          $label = $parent->label();
          $depth = $this->getTaxonomyDepth($parent);
    
          $parentsList[$depth] = array(
            "id" => $id,
            "label" => $label,
          );
        }
        // max depth is currently 5
        $level1 = $parentsList[1] ? $parentsList[1]["label"] : "";
        $level2 = $parentsList[2] ? $parentsList[2]["label"] : "";
        $level3 = $parentsList[3] ? $parentsList[3]["label"] : "";
        $level4 = $parentsList[4] ? $parentsList[4]["label"] : "";
        $level5 = $parentsList[5] ? $parentsList[5]["label"] : "";
        $termUrl = $this->generateTermSearchUrl($term);
        
        $row = [$term->id(), $level1, $level2, $level3, $level4, $level5, $termUrl];

        fputcsv($file, $row);
        $numTerms += 1;
      }

      fclose($file);

      $duration = (time()) - $start;
      $this->output()->writeln("$numTerms terms written to 'csv_files/earthdata_taxonomy_terms.csv' in $duration seconds.");

    } else {
      $this->output()->writeln('writetermscsv cancelled.');
    }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /* * * * * * * * *    Write Matches to csv     * * * * * * * * * */
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /**
   * Writes matching taxonomy terms to a csv file.
   *
   * @command taxonomy:write-matches-to-csv
   * @aliases writematchescsv
   *
   * @usage taxonomy:write-matches-to-csv
   */
  public function writeMatchesToCsv() {
    if ($this->io()->confirm("This will overwrite taxonomy_matches.csv with current matching terms.  Are you sure you want to proceed?")) {
      // ensure needed files exist.
      $search_taxonomy_file = "modules/custom/earthdata_utilities/csv_files/search_taxonomy_terms.csv";
      $esds_taxonomy_file = "modules/custom/earthdata_utilities/csv_files/earthdata_taxonomy_terms.csv";
      if (file_exists($search_taxonomy_file) && file_exists($esds_taxonomy_file)) {
        $start = time();
        $this->output()->writeln('Writing matching terms to csv file...');

        // prepare the file
        $taxonomy_matches = fopen("modules/custom/earthdata_utilities/csv_files/taxonomy_matches.csv", "w");
        fputcsv($taxonomy_matches, ['ID', 'Level 1', 'Level 2', 'Level 3', 'Level 4', 'Level 5', 'Search URL', 'Term URL']);
      
        $termMatches = $this->getSearchTermMatches();
        
        $written = 0;
        foreach ($termMatches["matches"] as $match) {
          if ($match[0] != "ID") {
              fputcsv($taxonomy_matches, $match);
              $written += 1;
          }
        }
      
        fclose($taxonomy_matches);

        $duration = (time()) - $start;
        $this->output()->writeln("writematchescsv complete.\n  $written matching terms written to /earthdata_utilities/csv_files/taxonomy_matches.csv\n  Completed in $duration seconds.");
      } else {
        $this->output()->writeln("Error: one or both of 'csv_files/search_taxonomy_terms.csv' and 'csv_files/earthdata_taxonomy_terms.csv' missing.\n  writematchescsv cancelled.");
      }
    } else {
      $this->output()->writeln('writematchescsv cancelled.');
    }
  }

  /**
   * Helper function
   * Iterates over search.earthdata terms
   * looks through ESDS terms for matching TermId or says "no match"
   * writes file with report.
   * 
   * @return array of 2 arrays: matches and non-matches
   */
  private function getSearchTermMatches() {
    $search_taxonomy = fopen("modules/custom/earthdata_utilities/csv_files/search_taxonomy_terms.csv", "r");
    $matches = [];
    $non_matches = [];
    $row = 1;
    if ($search_taxonomy !== FALSE) {
      while (($dataRow = fgetcsv($search_taxonomy, 1000, ",")) !== FALSE) {
        $term = $this->findSearchMatch($dataRow);
        if ($term == "no match") {
          $data = ["no match"];
          foreach ($dataRow as $item) {
            array_push($data, $item);
          }
          array_push($non_matches, $data);
        } else {
          $data = [];
          foreach ($term as $field) {
            array_push($data, $field);
          }
          array_push($data, ('https://www.earthdata.nasa.gov/taxonomy/term/' . $term[0]));
          array_push($matches, $data);
        }

        $row++;
      }
    }
    fclose($search_taxonomy);
    return array("matches" => $matches, "non_matches" => $non_matches);
  }

  /**
   * Helper function
   * Given a search.earthdata term row, iterates through earthdata_taxonomy_terms.csv terms and
   * checks to see if every level of data matches.  If so, returns the earthdata term row.
   * Otherwise, returns the string "no match."
   * 
   * @return array $data - an earthdata taxonomy term row.
   */
  private function findSearchMatch($dataRow) {
    $esds_taxonomy = fopen("modules/custom/earthdata_utilities/csv_files/earthdata_taxonomy_terms.csv", "r");
    $row = 1;
    $level1 = ($dataRow[0] === "Oceans") ? "ocean" : strtolower($dataRow[0]);
    $level2 = strtolower($dataRow[1]);
    $level3 = strtolower($dataRow[2]);
    $level4 = strtolower($dataRow[3]);
    $level5 = strtolower($dataRow[4]);
    if ($esds_taxonomy !== FALSE) {
      while (($data = fgetcsv($esds_taxonomy, 1000, ",")) !== FALSE) {
        $data1 = strtolower($data[1]);
        $data2 = strtolower($data[2]);
        $data3 = strtolower($data[3]);
        $data4 = strtolower($data[4]);
        $data5 = strtolower($data[5]);
        if (($data1 == $level1) && ($data2 == $level2) && ($data3 == $level3) && ($data4 == $level4) && ($data5 == $level5)) {
          $termId = $data[0];
          fclose($esds_taxonomy);
          return $data;
        }
        $row++;
      }
    }
    fclose($esds_taxonomy);
    return "no match";
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  /* * * * * * * * *    Write Comparison to csv    * * * * * * * * */
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  /**
   * Writes matching and non-matching taxonomy terms to a csv file.
   *
   * @command taxonomy:write-comparison-to-csv
   * @aliases writecomparisoncsv
   *
   * @usage taxonomy:write-comparison-to-csv
   */
  public function writeComparisonToCsv() {
    if ($this->io()->confirm("This will overwrite taxonomy_compare.csv with current terms comparison.  Are you sure you want to proceed?")) {
      // ensure needed files exist.
      $search_taxonomy_file = "modules/custom/earthdata_utilities/csv_files/search_taxonomy_terms.csv";
      $esds_taxonomy_file = "modules/custom/earthdata_utilities/csv_files/earthdata_taxonomy_terms.csv";
      if (file_exists($search_taxonomy_file) && file_exists($esds_taxonomy_file)) {
        $start = time();

        // prepare the file
        $taxonomy_compare = fopen("modules/custom/earthdata_utilities/csv_files/taxonomy_compare.csv", "w");
        fputcsv($taxonomy_compare, ['match?', 'source', 'ID', 'Level 1', 'Level 2', 'Level 3', 'Level 4', 'Level 5', 'Search URL', 'Term URL']);
      
        // $matches = [];
        // $non_matches = [];
        $this->output()->writeln('Getting matches and non-matches for search...');
        $searchTermMatches = $this->getSearchTermMatches();

        $this->output()->writeln('Getting matches and non-matches for esds...');
        $esdsTermMatches = $this->getESDSTermMatches();

        $this->output()->writeln('Writing term comparison csv file...');
        $numMatching = 0;
        $numSearchNonMatching = 0;
        $numESDSNonMatching = 0;
        foreach ($searchTermMatches["matches"] as $match) {
          if ($match[0] != "ID") {
            array_unshift($match, "esds");
            array_unshift($match, "MATCH");
            fputcsv($taxonomy_compare, $match);
            $numMatching += 1;
          }
        }
      
        foreach ($searchTermMatches["non_matches"] as $search_non_match) {
          if ($search_non_match[0] != "ID") {
            $search_non_match[0] = "(no id)";
            array_unshift($search_non_match, "search");
            array_unshift($search_non_match, "no match");
            fputcsv($taxonomy_compare, $search_non_match);
            $numSearchNonMatching += 1;
          }
        }

        foreach ($esdsTermMatches["non_matches"] as $esds_non_match) {
          if ($esds_non_match[0] != "ID") {
            array_unshift($esds_non_match, "esds");
            array_unshift($esds_non_match, "no match");
            fputcsv($taxonomy_compare, $esds_non_match);
            $numESDSNonMatching += 1;
          }
        }
      
        fclose($taxonomy_compare);

        $duration = (time()) - $start;
        $message = "\nwritecomparisoncsv complete.\n  * Term comparison written to /earthdata_utilities/csv_files/taxonomy_compare.csv";
        $message .= "\n  * Found $numMatching matching terms.";
        $message .= "\n  * $numSearchNonMatching terms from search.earthdata.nasa.gov did not match an ESDS term.";
        $message .= "\n  * $numESDSNonMatching terms from ESDS did not match a search.earthdata.nasa.gov term.";
        $message .= "\n  * Completed in $duration seconds.";

        $this->output()->writeln($message);

      } else {
        $this->output()->writeln("Error: one or both of 'csv_files/search_taxonomy_terms.csv' and 'csv_files/earthdata_taxonomy_terms.csv' missing.\n  writecomparisoncsv cancelled.");
      }

    } else {
      $this->output()->writeln('writecomparisoncsv cancelled.');
    }
  
  }

  /**
   * Helper function
   * Iterates over earthdata terms
   * looks through search.earthdata terms for matching TermId or says "no match"
   * writes file with report.
   * 
   * @return array of 2 arrays: matches and non-matches
   */
  private function getESDSTermMatches() {
    $esds_taxonomy = fopen("modules/custom/earthdata_utilities/csv_files/earthdata_taxonomy_terms.csv", "r");
    $matches = [];
    $non_matches = [];
    $row = 1;
    if ($esds_taxonomy !== FALSE) {
      while (($dataRow = fgetcsv($esds_taxonomy, 1000, ",")) !== FALSE) {
        $term = $this->findESDSMatch($dataRow);
        if ($term == "no match") {
          $data = [];
          foreach ($dataRow as $item) {
            array_push($data, $item);
          }
          array_push($non_matches, $data);
        } else {
          $data = ["match"];
          foreach ($term as $field) {
            array_push($data, $field);
          }
          array_push($matches, $data);
        }

        $row++;
      }
    }
    fclose($esds_taxonomy);
    return array("matches" => $matches, "non_matches" => $non_matches);
  }

  /**
   * Helper function
   * Given an ESDS term row, iterates through search_taxonomy_terms.csv terms and
   * checks to see if every level of data matches.  If so, returns the earthdata term row.
   * Otherwise, returns the string "no match."
   * 
   * @return array $data - a search taxonomy term row.
   *  or string "no match"
   */
  private function findESDSMatch($dataRow) {
    $search_taxonomy = fopen("modules/custom/earthdata_utilities/csv_files/search_taxonomy_terms.csv", "r");
    $row = 1;
    $level1 = strtolower($dataRow[1]);
    $level2 = strtolower($dataRow[2]);
    $level3 = strtolower($dataRow[3]);
    $level4 = strtolower($dataRow[4]);
    $level5 = strtolower($dataRow[5]);
    if ($search_taxonomy !== FALSE) {
      while (($data = fgetcsv($search_taxonomy, 1000, ",")) !== FALSE) {
        $data1 = ($data[0] === "Oceans") ? "ocean" : strtolower($data[0]);
        $data2 = strtolower($data[1]);
        $data3 = strtolower($data[2]);
        $data4 = strtolower($data[3]);
        $data5 = strtolower($data[4]);
        if (($data1 == $level1) && ($data2 == $level2) && ($data3 == $level3) && ($data4 == $level4) && ($data5 == $level5)) {
          $termId = $dataRow[0];
          fclose($search_taxonomy);
          return $data;
        }
        $row++;
      }
    }
    fclose($search_taxonomy);
    return "no match";
  }

}
