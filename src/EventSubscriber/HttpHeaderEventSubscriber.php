<?php
/**
 * @file
 * Contains Drupal\earthdata_utilities\EventSubscriber\HttpHeaderEventSubscriber.
 */

namespace Drupal\earthdata_utilities\EventSubscriber;

use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event Subscriber HttpHeaderEventSubscriber.
 */
class HttpHeaderEventSubscriber implements EventSubscriberInterface {

  /**
   * Code that should be triggered on event specified.
   */
  public function onRespond(FilterResponseEvent $event) {
    // The RESPONSE event occurs once a response was created for replying to a request.
    // Add extra HTTP headers here.
    $csp_header_value = $this->get_csp_header_value();
    $is_anonymous = \Drupal::currentUser()->isAnonymous();
    $response = $event->getResponse();

    # Add the Content-Security-Policy HTTP header, but not for Drupal's admin pages.
    if ($is_anonymous) {
      $response->headers->set('Content-Security-Policy', $csp_header_value);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    // For this example I am using KernelEvents constants (see below a full list).
    $events[KernelEvents::RESPONSE][] = ['onRespond'];
    return $events;
  }

  /**
   * Returns the Content-Security-Policy header value.
   */
  private function get_csp_header_value() {
    return "upgrade-insecure-requests; object-src 'none'; script-src 'self' 'unsafe-eval' www.google.com www.gstatic.com https://assets.pinterest.com https://www.google-analytics.com 'sha256-EeeULpREplDSZUSVW97YrBpzPCltPT/BgVivzUtuZwM=' 'sha256-w8Sow0sTeay1IhIbcEPNolCDu2M0Tavi1erOmdcgJYE=' cdn.jsdelivr.net cdnjs.cloudflare.com dap.digitalgov.gov https://cdn.jsdelivr.net https://cdnjs.cloudflare.com https://polyfill.io https://rebilly.github.io https://static.addtoany.com https://unpkg.com mdbootstrap.com script.crazyegg.com static.ctctcdn.com www.googletagmanager.com; script-src-attr 'self'; script-src-elem 'self' www.google.com www.gstatic.com https://assets.pinterest.com https://www.google-analytics.com https://www.youtube.com 'sha256-w8Sow0sTeay1IhIbcEPNolCDu2M0Tavi1erOmdcgJYE=' 'sha256-EeeULpREplDSZUSVW97YrBpzPCltPT/BgVivzUtuZwM=' 'sha256-rtaVU57dLbRdkXCugTr49x7HJRqjTwe5YoVCy2M4dDE=' 'sha256-kl2nf6jTgbyz1+EJohrUkb9c8TLB8V8ycI1au5okobs=' 'sha256-gjnw1iqnzFlCo3IKNENFiTkV/IzZfNHN2sQzNsJj+IY=' 'sha256-LRUIZNVwrnCYR4Bv51LqrxPsZi+DJwqT6Mw4txzMssw=' 'sha256-tU9n/6S064bg/WCR7IOABQpmPu9LWRntxR8xEyqlE1k=' 'sha256-UvaZ9dnD1vbqwGnEW9cIXalSGus6PH9AjVjCtHkaV8M=' 'sha256-vnhTSsDm11lp1CkhVnY8Q7eDKNMS15mbpiuBEDeipyM=' 'sha256-zAjiNTl5IvSf3pdxydcAaShAjB7Ym0irlUZ3apj+26A=' 'sha256-CV54lQexbqK41oQut9EF63lZCWMSkD19f2V1iSaus0w=' 'sha256-M+88Wh4bjsDdiJoNEgQbkyfyqCGwx9WKsa897sfM4ZQ=' 'sha256-2M400TBOWIAG0gL8q8iuh+PK0TW7ZRMhrvE7BWDi1os=' 'sha256-/g/U1jIWK1Qv4D8fY7waf5qGfLtait/l8X7131Igt24=' 'sha256-u8T6NVagggpi9dvdxoTOKipUQAu4VIZg58vZmle5IWk=' cdn.jsdelivr.net cdnjs.cloudflare.com dap.digitalgov.gov https://cdn.jsdelivr.net https://cdnjs.cloudflare.com https://polyfill.io https://rebilly.github.io https://static.addtoany.com https://unpkg.com mdbootstrap.com script.crazyegg.com static.ctctcdn.com www.googletagmanager.com; style-src 'self' 'unsafe-inline' https://static.ctctcdn.com cdn.jsdelivr.net fonts.googleapis.com https://cdn.jsdelivr.net https://cdnjs.cloudflare.com mdbootstrap.com stackpath.bootstrapcdn.com use.fontawesome.com; worker-src 'self' blob: https://script.crazyegg.com; frame-ancestors 'self'";
  }

}
