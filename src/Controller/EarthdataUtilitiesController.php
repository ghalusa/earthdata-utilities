<?php

namespace Drupal\earthdata_utilities\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Yaml\Yaml;
use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\Component\Render\FormattableMarkup;

/**
 * Returns responses for Earthdata Utilities routes.
 */
class EarthdataUtilitiesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function build() {

    $build['content'] = [
      '#markup' => $this->t('<h2>Example</h2><p>It works!</p>'),
    ];

    return $build;
  }

  /**
  * Content Updates
  * Route: /admin/config/earthdata/content-updates
  *
  * Create a report of differences between Conduit's content and Drupal's content.
  */
  public function contentUpdates() {

    module_load_include('inc', 'earthdata_migrate', 'includes/utilities');
    module_load_include('php', 'earthdata_utilities', 'includes/simplediff');

    $rows = $build = [];

    $header = [
      ['data' => t('Title'), 'field' => 'title', 'sort' => 'asc', 'width'=>'20%'],
      ['data' => t('Conduit URL'), 'width'=>'10%'],
      ['data' => t('Drupal URL'), 'width'=>'10%'],
      ['data' => t('Created Date'), 'field' => 'created_at'],
      ['data' => t('Version Date'), 'field' => 'updated_at'],
      ['data' => t('Differences'), 'width'=>'40%'],
    ];

    $sql_source_fields = [
      'id',
      'type',
      'created_at',
      'updated_at',
      'title',
      'slug',
      'ancestry',
      'author',
      'content',
      'project_id',
      'is_draft',
      'is_published',
      'state',
    ];

    // // Query for one record by ID.
    // // Does not pertian to this effort, but handy for a quick lookup.
    // $record = $this->getOneRecord(1440, $sql_source_fields);
    // dd($record);

    $conduit_records = $this->getRecords($sql_source_fields, $header);

    // dd($conduit_records);

    if (!empty($conduit_records)) {

      // Loop through Condit's "pages" records.
      foreach ($conduit_records as $key => $conduit_record) {

        $conduit_body = $conduit_body_revision = '';
        $drupal_url = 'Record not in Drupal';

        // Decode Conduit's JSON content field.
        $conduit_record_decoded = $this->edJsonDecode($conduit_record->content);

        // Query Conduit's database for the latest version from the "versions" table.
        // Note: If there is no revision record in Conduit's database, then there are no revisions.
        $conduit_latest_version = $this->getVersion($conduit_record->id);

        // Query Conduit's database for the latest version from the "versions" table.
        // Note: If there is no revision record in Conduit's database, then there are no revisions and we skip processing.
        if (!empty($conduit_latest_version) && ($conduit_latest_version->created_at >= '2021-07-15 00:00:00.000000')) {

          // Remove the timestamps and convert the YAML to a PHP array.
          $conduit_record_version = $this->processYaml($conduit_latest_version->object);
          // Sort the keys alphabetically.
          ksort($conduit_record_version);

          // Query for the Drupal node by title.
          $drupal_node = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
            'title' => trim($conduit_record->title),
          ]);

          if (!empty($drupal_node)) {
            $key = array_keys($drupal_node);
            $drupal_record = $drupal_node[$key[0]];
          }

          // Get Conduit's URL alias.
          $conduit_url_alias = ed_get_url_alias($conduit_record->id);
          // Get Drupal's URL alias.
          $drupal_url_alias = !empty($drupal_node) ? \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $drupal_record->id()) : NULL;

          $title = new FormattableMarkup('<strong>@title</strong>',['@title' => $conduit_record->title]);
          $conduit_url = new FormattableMarkup('<a href="https://earthdata.nasa.gov@conduit_url" target="_blank">View Conduit Page</a>',['@conduit_url' => $conduit_url_alias]);

          if (!empty($drupal_url_alias)) {
            $drupal_url = new FormattableMarkup('<a href="https://d3ii0cv1in65ji.cloudfront.net@drupal_url" target="_blank">View Drupal Page</a>',['@drupal_url' => $drupal_url_alias]);
          }

          if (array_key_exists('body', $conduit_record_version['content_decoded'])) {
            // Remove carriage returns from Conduit's body revision.
            $conduit_body_revision = str_replace(array("\n", "\t", "\r"), '', $conduit_record_version['content_decoded']['body']);
            // Remove HTML tags from Conduit's body revision.
            $conduit_body_revision = strip_tags($conduit_record_version['content_decoded']['body'], '<p><a><strong><em><br><img><table><th><tr><td>');
          }

          if (array_key_exists('body', $conduit_record_decoded)) {
            // Remove carriage returns from Conduit's body.
            $conduit_body = str_replace(array("\n", "\t", "\r"), '', $conduit_record_decoded['body']);
            // Remove HTML tags from Conduit's body.
            $conduit_body = strip_tags($conduit_record_decoded['body'], '<p><a><strong><em><br><img><table><th><tr><td>');
          }

          // Perform a comparison to ensure differences exist.
          $differences = htmlDiff($conduit_body_revision, $conduit_body);

          $differences = str_replace('<ins>', '<span style="background-color: rgb(179, 222, 193);">', $differences);
          $differences = str_replace('</ins>', '</span>', $differences);
          $differences = str_replace('<del>', '<span style="background-color: rgb(242, 195, 194);">', $differences);
          $differences = str_replace('</del>', '</span>', $differences);

          $diff = new FormattableMarkup($differences,[]);

          if (strstr($differences, '<span style="background-color: rgb(179, 222, 193);">') || strstr($differences, '<span style="background-color: rgb(242, 195, 194);">')) {

            // Construct the table rows.
            $rows[] = [
              [ 'data' => $title, 'style' => 'vertical-align: top;' ],
              [ 'data' => $conduit_url, 'style' => 'vertical-align: top;' ],
              [ 'data' => $drupal_url, 'style' => 'vertical-align: top;' ],
              [ 'data' => $conduit_record->created_at, 'style' => 'vertical-align: top;' ],
              [ 'data' => $conduit_latest_version->created_at, 'style' => 'vertical-align: top;' ],
              [ 'data' => $diff, 'style' => 'word-break: break-word;' ], // word-wrap: break-word;
            ];

          } else {

            // Construct the table rows.
            $rows[] = [
              [ 'data' => $title, 'style' => 'vertical-align: top;' ],
              [ 'data' => $conduit_url, 'style' => 'vertical-align: top;' ],
              [ 'data' => $drupal_url, 'style' => 'vertical-align: top;' ],
              [ 'data' => $conduit_record->created_at, 'style' => 'vertical-align: top;' ],
              [ 'data' => $conduit_latest_version->created_at, 'style' => 'vertical-align: top;' ],
              [ 'data' => 'No changes to the body detected.' ],
            ];

          }

        }

      }

    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes'=> ['style' => 'IdforRow'],
      '#sticky' => TRUE,
      '#empty' => t('No Conduit updates found.'),
    ];

    $build['pager'] = [
      '#type' => 'pager'
    ];

    return $build;
  }

  /**
  * Content Updates CSV
  * Route: /admin/config/earthdata/content-updates-csv
  *
  * Create a report of differences between Conduit's content and Drupal's content.
  */
  public function contentUpdatesCsv() {

    module_load_include('inc', 'earthdata_migrate', 'includes/utilities');
    module_load_include('php', 'earthdata_utilities', 'includes/simplediff');

    $rows = $build = [];

    // Start using PHP's built in file handler functions to create a temporary file.
    $handle = fopen('php://temp', 'w+');

    // Set up the header that will be displayed as the first line of the CSV file.
    $header = [
      'Title',
      'Conduit URL',
      'Drupal URL',
      'Created Date',
      'Version Date',
      'Differences',
    ];
    // Add the header as the first line of the CSV.
    fputcsv($handle, $header);

    $sql_source_fields = [
      'id',
      'type',
      'created_at',
      'updated_at',
      'title',
      'slug',
      'ancestry',
      'author',
      'content',
      'project_id',
      'is_draft',
      'is_published',
      'state',
    ];

    // // Query for one record by ID.
    // // Does not pertian to this effort, but handy for a quick lookup.
    // $record = $this->getOneRecord(1440, $sql_source_fields);
    // dd($record);

    $conduit_records = $this->getAllRecords($sql_source_fields, $header);

    // dd($conduit_records);

    if (!empty($conduit_records)) {

      // Loop through Condit's "pages" records.
      foreach ($conduit_records as $key => $conduit_record) {

        $conduit_body = $conduit_body_revision = '';
        $drupal_url = 'Record not in Drupal';

        // Decode Conduit's JSON content field.
        $conduit_record_decoded = $this->edJsonDecode($conduit_record->content);

        // Query Conduit's database for the latest version from the "versions" table.
        // Note: If there is no revision record in Conduit's database, then there are no revisions.
        $conduit_latest_version = $this->getVersion($conduit_record->id);

        // Query Conduit's database for the latest version from the "versions" table.
        // Note: If there is no revision record in Conduit's database, then there are no revisions and we skip processing.
        if (!empty($conduit_latest_version) && ($conduit_latest_version->created_at >= '2021-07-15 00:00:00.000000')) {

          // Remove the timestamps and convert the YAML to a PHP array.
          $conduit_record_version = $this->processYaml($conduit_latest_version->object);
          // Sort the keys alphabetically.
          ksort($conduit_record_version);

          // Query for the Drupal node by title.
          $drupal_node = \Drupal::entityTypeManager()->getStorage('node')->loadByProperties([
            'title' => trim($conduit_record->title),
          ]);

          if (!empty($drupal_node)) {
            $key = array_keys($drupal_node);
            $drupal_record = $drupal_node[$key[0]];
          }

          // Get Conduit's URL alias.
          $conduit_url_alias = ed_get_url_alias($conduit_record->id);
          // Get Drupal's URL alias.
          $drupal_url_alias = !empty($drupal_node) ? \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $drupal_record->id()) : NULL;

          if (!empty($drupal_url_alias)) {
            $drupal_url = 'https://d3ii0cv1in65ji.cloudfront.net' . $drupal_url_alias;
          }

          if (array_key_exists('body', $conduit_record_version['content_decoded'])) {
            // Remove carriage returns from Conduit's body revision.
            $conduit_body_revision = str_replace(array("\n", "\t", "\r"), '', $conduit_record_version['content_decoded']['body']);
            // Remove HTML tags from Conduit's body revision.
            $conduit_body_revision = strip_tags($conduit_record_version['content_decoded']['body'], '<p><a><strong><em><br><img><table><th><tr><td>');
          }

          if (array_key_exists('body', $conduit_record_decoded)) {
            // Remove carriage returns from Conduit's body.
            $conduit_body = str_replace(array("\n", "\t", "\r"), '', $conduit_record_decoded['body']);
            // Remove HTML tags from Conduit's body.
            $conduit_body = strip_tags($conduit_record_decoded['body'], '<p><a><strong><em><br><img><table><th><tr><td>');
          }

          // Perform a comparison to ensure differences exist.
          $differences = htmlDiff($conduit_body_revision, $conduit_body);

          $differences = str_replace('<ins>', '<span style="background-color: rgb(179, 222, 193);">', $differences);
          $differences = str_replace('</ins>', '</span>', $differences);
          $differences = str_replace('<del>', '<span style="background-color: rgb(242, 195, 194);">', $differences);
          $differences = str_replace('</del>', '</span>', $differences);

          if (strstr($differences, '<span style="background-color: rgb(179, 222, 193);">') || strstr($differences, '<span style="background-color: rgb(242, 195, 194);">')) {

            // Add a record to the next line of the CSV.
            fputcsv($handle, [
              $conduit_record->title,
              'https://earthdata.nasa.gov' . $conduit_url_alias,
              'https://d3ii0cv1in65ji.cloudfront.net' . $drupal_url,
              $conduit_record->created_at,
              $conduit_latest_version->created_at,
              '']
            );

          } else {

            // Add a record to the next line of the CSV.
            fputcsv($handle,
              [$conduit_record->title,
              'https://earthdata.nasa.gov' . $conduit_url_alias,
              $drupal_url,
              $conduit_record->created_at,
              $conduit_latest_version->created_at,
              'No changes to the body detected.']
            );

          }

        }

      }

    }

    // Reset where we are in the CSV.
    rewind($handle);
    // Retrieve the data from the file handler.
    $csv_data = stream_get_contents($handle);
    // Close the file handler. We are not storing this file anywhere in the filesystem.
    fclose($handle);

    // Prevent from being cached.
    \Drupal::service('page_cache_kill_switch')->trigger();

    // Return it as a response.
    $response = new Response();
    // Set the header options.
    $response->headers->set('Content-Type', 'text/csv');
    $response->headers->set('Content-Disposition', 'attachment; filename="content-updates.csv"');
    // Physically add the CSV data.
    $response->setContent($csv_data);

    return $response;
  }

  /**
   * JSON Decode
   *
   * JSON decode the content.
   * @param string $json
   * @return array
   */
  public function edJsonDecode($json) {

    $data = [];

    if (!empty($json)) {
      $data = json_decode($json, TRUE);
    }

    return $data;
  }

  /**
   * Process YAML
   *
   * @param string $json
   * @return array
   */
  public function processYaml($yaml = NULL) {

    $data = [];

    if(empty($yaml)) return $data;

    // Clean-up the timestamps to avoid:
    // Exception: DateTime::__construct(): Failed to parse time string (2021-11-29 19:48:58.525321000 Z) at position 11 (1):
    // The timezone could not be found in the database in DateTime->__construct()
    // (line 678 of /var/www/html/vendor/symfony/yaml/Inline.php).

    // Clean-up published_at.
    $pattern = "~(?s)published_at:.*?\n.*?\n.*?\n  time:~";
    $replacement = "published_at:";
    $cleaned = preg_replace($pattern, $replacement, $yaml);
    $pattern = "/published_at: (.*)\n/";
    $replacement = 'published_at: "$1"' . "\n";
    $cleaned = preg_replace($pattern, $replacement, $cleaned);
    $pattern = "/published_at: \"(.*) Z\"/";
    $replacement = 'published_at: "$1"';
    $cleaned = preg_replace($pattern, $replacement, $cleaned);

    // Clean-up updated_at.
    $pattern = "~(?s)updated_at:.*?\n.*?\n.*?\n  time:~";
    $replacement = "updated_at:";
    $cleaned = preg_replace($pattern, $replacement, $cleaned);
    $pattern = "/updated_at: (.*)\n/";
    $replacement = 'updated_at: "$1"' . "\n";
    $cleaned = preg_replace($pattern, $replacement, $cleaned);
    $pattern = "/updated_at: \"(.*) Z\"/";
    $replacement = 'updated_at: "$1"';
    $cleaned = preg_replace($pattern, $replacement, $cleaned);

    // Clean-up created_at.
    $pattern = "~(?s)created_at:.*?\n.*?\n.*?\n  time:~";
    $replacement = "created_at:";
    $cleaned = preg_replace($pattern, $replacement, $cleaned);
    $pattern = "/created_at: (.*)\n/";
    $replacement = 'created_at: "$1"' . "\n";
    $cleaned = preg_replace($pattern, $replacement, $cleaned);
    $pattern = "/created_at: \"(.*) Z\"/";
    $replacement = 'created_at: "$1"';
    $cleaned = preg_replace($pattern, $replacement, $cleaned);

    // Convert the YAML to a PHP array.
    $data = Yaml::parse($cleaned, Yaml::PARSE_CUSTOM_TAGS);

    if (!empty($data)) {
      $data['content_decoded'] = $this->edJsonDecode($data['content']);
    }

    return $data;
  }

  public function getRecords($sql_source_fields = [], $header = []) {

    $data = [];

    if(empty($sql_source_fields)) return $data;

    // Switch to the Conduit database.
    \Drupal\Core\Database\Database::setActiveConnection('conduit');
    $db = \Drupal\Core\Database\Database::getConnection();

    try {

      // Query Conduit's database.
      $query = $db->select('pages','p')
        ->fields('p', $sql_source_fields)
        ->condition('updated_at', '2021-07-15 00:00:00.000000', '>')
        ->condition('is_draft', false, '=');

      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                    ->orderByHeader($header);
      // Limit the rows to 100 for each page.
      $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
                ->limit(10);
      $data = $pager->execute();

    } catch (\Exception $e) {
      // No database
      $data = [];
    }

    // Switch back to the default database.
    \Drupal\Core\Database\Database::setActiveConnection();

    return $data;
  }

  public function getAllRecords($sql_source_fields = []) {

    $data = [];

    if(empty($sql_source_fields)) return $data;

    // Switch to the Conduit database.
    \Drupal\Core\Database\Database::setActiveConnection('conduit');
    $db = \Drupal\Core\Database\Database::getConnection();

    try {

      // Query Conduit's database.
      $data = $db->select('pages','p')
        ->fields('p', $sql_source_fields)
        ->condition('updated_at', '2021-07-15 00:00:00.000000', '>')
        ->condition('is_draft', false, '=')
        ->orderBy('title', 'DESC')
        ->execute()
        ->fetchAll();

    } catch (\Exception $e) {
      // No database
      $data = [];
    }

    // Switch back to the default database.
    \Drupal\Core\Database\Database::setActiveConnection();

    return $data;
  }

  public function getVersion($id = '') {

    $data = [];

    if(empty($id)) return $data;

    // Switch to the Conduit database.
    \Drupal\Core\Database\Database::setActiveConnection('conduit');
    $db = \Drupal\Core\Database\Database::getConnection();

    // Query Conduit's database for the latest version from the "versions" table.
    $data = $db->select('versions','v')
      ->fields('v')
      ->condition('item_id', $id)
      ->condition('event', 'update')
      ->orderBy('created_at', 'DESC')
      ->range(0, 1)
      ->execute()
      ->fetch();

    // Switch back to the default database.
    \Drupal\Core\Database\Database::setActiveConnection();

    return $data;
  }

  public function getOneRecord($id = '', $sql_source_fields = []) {

    $data = [];

    if(empty($id)) return $data;
    if(empty($sql_source_fields)) return $data;

    // Switch to the Conduit database.
    \Drupal\Core\Database\Database::setActiveConnection('conduit');
    $db = \Drupal\Core\Database\Database::getConnection();

    $data = $db->select('pages','p')
      ->fields('p', $sql_source_fields)
      ->condition('id', $id, '=')
      ->execute()
      ->fetch();

    $data->content_decoded = $this->edJsonDecode($data->content);

    // Switch back to the default database.
    \Drupal\Core\Database\Database::setActiveConnection();

    return $data;

  }

  // ESDS-870: Scan the site for broken links
  public function cleanUrlRedirects() {

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    // Start using PHP's built in file handler functions to create a temporary file.
    $handle = fopen('php://temp', 'w+');

    // Set up the header that will be displayed as the first line of the CSV file.
    $header = [
      'Node ID',
      'Alias',
    ];
    // Add the header as the first line of the CSV.
    fputcsv($handle, $header);

    foreach ($ids as $key => $nid) {

      // Get the node's URL alias.
      // Example: $alias_string = '/eosdis/system-performance/esdis-metrics';
      $alias_string = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
      $alias = ltrim($alias_string, '/');

      // Check for corresponding URL redirects.
      $redirect_results = \Drupal::entityTypeManager()->getStorage('redirect')->loadByProperties(['redirect_source' => $alias]);

      // If URL redirects exist, remove them.
      if (!empty($redirect_results)) {
        foreach ($redirect_results as $k => $entity) {
          $entity->delete();
          // Add a record to the next line of the CSV.
          fputcsv($handle, [$nid, $alias]);
        }
      }

    }

    // Reset where we are in the CSV.
    rewind($handle);
    // Retrieve the data from the file handler.
    $csv_data = stream_get_contents($handle);
    // Close the file handler. We are not storing this file anywhere in the filesystem.
    fclose($handle);

    // Prevent from being cached.
    \Drupal::service('page_cache_kill_switch')->trigger();

    if (strlen($csv_data) > 16) {
      // Return it as a response.
      $response = new Response();
      // Set the header options.
      $response->headers->set('Content-Type', 'text/csv');
      $response->headers->set('Content-Disposition', 'attachment; filename="redirects-removed.csv"');
      // Physically add the CSV data.
      $response->setContent($csv_data);

      return $response;

    } else {

      $build['content'] = [
        '#markup' => '<h2>Clean URL Redirects Report</h2><p>No redirects removed.</p>',
      ];

      return $build;

    }

  }

  /**
  * ESDS-870: Scan the site for broken links
  * Route: /earthdata-utilities/fix-files
  */
  public function fixFilesNotFound() {

    // // Get all entity types.
    // dd(array_keys(\Drupal::entityTypeManager()->getDefinitions()));

    // // Remove unused file entities.
    // $this->removeUnusedFileEntities();

    $messages = [];
    $host = \Drupal::request()->getHost();
    $filesystem = \Drupal::service('file_system');

    // Absolute path to this module.
    $module_handler = \Drupal::service('module_handler');
    $module_path = $module_handler->getModule('earthdata_utilities')->getPath();
    $absolute_path = \Drupal::service('file_system')->realpath($module_path);

    $json_path = $absolute_path . '/json/files-not-found.json';

    $json = json_decode(file_get_contents($json_path), true);

    // Loop through file urls.
    foreach ($json as $key => $value) {

      // $value['url'] example:
      // <a href="https://dev.openearthdata.com/sites/default/files/imported/MPARWG_Oct09_Minutes_091030.doc">https://dev.openearthdata.com/sites/default/files/imported/MPARWG_Oct09_Minutes_091030.doc</a>

      // Extract the file url from the href attribute.
      preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', $value['url'], $result);

      // Execute processing if the result isn't empty and array key 1 exists.
      if (!empty($result) && array_key_exists(1, $result)) {

        // Files path
        if (strstr($result[1], '/files/')) {

          $file_name = $filesystem->basename($result[1]);
          $file_url = 'https://earthdata.nasa.gov/files/' . $file_name;
          // Extract the node url path/alias from the href attribute.
          preg_match('~<a .*?href=[\'"]+(.*?)[\'"]+.*?>(.*?)</a>~ims', $value['nothing'], $node_alias);

          // Download the file from production and save it into Drupal's filesystem.
          $file_data = file_get_contents($file_url, TRUE);

          // Error handling.
          if ($file_data === FALSE) {
            $messages[] = 'Error: File not found - ' . $file_url . ' | ' . 'Source URL - ' . $node_alias[1];
          }

          // Only process if the $file_data is retrieveable.
          if ($file_data !== FALSE) {

            // Save the file.
            $saved_file = file_save_data($file_data, 'public://imported/' . urldecode($file_name), \Drupal\Core\File\FileSystemInterface::EXISTS_ERROR);

            // Error handling.
            if ($saved_file === FALSE) {
              $messages[] = 'Error: File not saved - ' . $file_url . ' | ' . 'Source URL - https://' . $host . '/' . $node_alias[1];
            }

            // Set the file mimetype.
            $file_mimetype = mime_content_type('public://imported/' . urldecode($file_name));

            // Create the image media entity with the saved file entity.
            if ($saved_file && !strstr($file_mimetype, 'image')) {

              // Create the file media entity with the saved file entity.
              $media = Media::create([
                'bundle' => 'document',
                'uid' => \Drupal::currentUser()->id(),
                'field_media_document' => [
                  'target_id' => $saved_file->id(),
                ],
               ]);

              $media->setName( $saved_file->getFilename() )->setPublished(TRUE)->save();

              $messages[] = 'Success: File added - https://' . $host . '/sites/default/files/imported/' . urldecode($file_name) . ' | ' . 'Source URL - https://' . $host . '/' . $node_alias[1];
            }

            // Update the body HTML (field_content - paragraphs entity references).
            //
            // Get the node's real path. (e.g. /node/1875).
            $node_path = \Drupal::service('path_alias.manager')->getPathByAlias($node_alias[1]);

            // dump($node_alias[1] . ' - ' . $node_path);

            if(preg_match('/node\/(\d+)/', $node_path, $matches)) {

              // Load the node entity.
              $node = \Drupal\node\Entity\Node::load($matches[1]);

              // Loop through the field_content field's referenced paragraphs entities.
              foreach ($node->field_content->referencedEntities() as $paragraph) {
                // Replace the broken file URL with the new file URL and save the paragraph entity.
                if (!strstr($result[1], '/sites/default/files/')) {
                  $paragraph->field_text_content->value = str_ireplace('/files/' . $file_name, '/sites/default/files/imported/' . urldecode($file_name), $paragraph->field_text_content->value);
                }

                $paragraph->save();
              }

            }

          }

        }

      }

    } // foreach ends

    $messages_html = '';

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . '<li>' . $mv . '</li>' . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>File 404s Clean-up Report</h2>' . "\n" . '<ul>' . $messages_html . '</ul>',
    ];

    return $build;
  }

  public function removeUnusedFileEntities() {

    $file_usage = \Drupal::service('file.usage');

    // Get all files ids.
    $query = \Drupal::entityTypeManager()->getStorage('file')->getQuery();
    $fids = $query->execute();

    // dd($fids);

    // Loop all fids and load files by fid.
    foreach ($fids as $fid) {
      $file = \Drupal\file\Entity\File::load($fid);
      $usage = $file_usage->listUsage($file);
      // Check if file not used.
      if (count($usage) == 0) {
        $file->delete();
      }
    }

    dd('Unused files deleted!');
  }

  /**
   * ESDS-870: Fixes "Mismatched entity and/or field definitions" reported in admin/reports/status.
   * Route: /earthdata-utilities/update-mismatched-entity-and-or-field-definitions
   *
   * WARNING: Please test before executing in a production environment due to possible data loss.
   */
  public function updateMismatchedEntityAndOrFieldDefinitions() {

    $entity_type_manager = \Drupal::entityTypeManager();
    $entity_type_manager->clearCachedDefinitions();

    $entity_type_ids = [];
    $change_summary = \Drupal::service('entity.definition_update_manager')->getChangeSummary();
    foreach ($change_summary as $entity_type_id => $change_list) {
      $entity_type = $entity_type_manager->getDefinition($entity_type_id);
      \Drupal::entityDefinitionUpdateManager()->installEntityType($entity_type);
      $entity_type_ids[] = $entity_type_id;
    }

    $markup = '<p>No updates required.</p>';

    if (!empty($entity_type_ids)) {
      $markup = '<p>Installed/Updated the entity type(s): ' . implode(', ', $entity_type_ids) . '</p>';
    }

    $build['content'] = [
      '#markup' => '<h2>Fixed "Mismatched entity and/or field definitions" Report</h2>' . "\n" . $markup,
    ];

    return $build;
  }

  /**
   * ESDS-870: Scan and remediate absolute links
   * Route: /earthdata-utilities/remove-absolute-links
   */
  public function removeAbsoluteLinks() {

    $messages = [];

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();
    sort($ids);

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);

      // Loop through the field_content field's referenced paragraphs entities.
      foreach ($node->field_content->referencedEntities() as $paragraph) {
        // Replace the absolute link with nothing.
        if (strstr($paragraph->field_text_content->value, 'https://earthdata.nasa.gov') || strstr($paragraph->field_text_content->value, 'http://earthdata.nasa.gov')) {
          // Replace absolute URLs with nothing.
          $paragraph->field_text_content->value = str_replace('https://earthdata.nasa.gov', '', $paragraph->field_text_content->value);
          $paragraph->field_text_content->value = str_replace('http://earthdata.nasa.gov', '', $paragraph->field_text_content->value);
          // Save the paragraph entity.
          $paragraph->save();
          // Get the node's URL alias.
          // Example: '/foo/bar';
          $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);
          // Set the message.
          $messages[] = 'Node ID: ' . $nid . ' | ' . '<a href="' . $alias . '" target="_blank">' . $node->getTitle() . '</a>';
        }
      }
    }

    // Set messages.
    $messages_html = '';

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . '<li>' . $mv . '</li>' . "\n";
    }

    if (!empty($messages_html)) {
      $messages_html = '<ul>' . $messages_html . '</ul>';
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<p>No absolute links found.</p>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>Removed Absolute Links</h2>' . "\n" . $messages_html,
    ];

    return $build;
  }

  /**
   * ESDS-870: Scan and remediate assets pointing to the CDN
   * Route: /earthdata-utilities/remove-content-delivery-network-assets
   */
  public function removeContentDeliveryNetworkAssets() {

    $messages = [];

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);

      if (!empty($node) && isset($node->field_content)) {
        // Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {
          // Replace the absolute link with nothing.
          if (strstr($paragraph->field_text_content->value, 'https://cdn.earthdata.nasa.gov') || strstr($paragraph->field_text_content->value, 'http://cdn.earthdata.nasa.gov')) {
            $messages[] = '<li><a href="https://d3ii0cv1in65ji.cloudfront.net/node/' . $node->id() . '/edit" target="_blank">' . $node->getTitle() . '</a></li>';
          }
        }
      }

    }

    $messages_html = '';
    $info = "\n" . '<p>A report of pages containing references to the CDN - https://cdn.earthdata.nasa.gov/.</p>' . "\n";

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>Content Delivery Network Assets Report</h2>' . "\n" . $info . "\n" . '<ol>' . $messages_html . '</ol>' . "\n",
    ];

    return $build;
  }

  /**
   * URLs Containing One URL Part
   * Examples: /hazards-and-disasters, /foo-bar, etc...
   * Route: /admin/config/earthdata/urls-containing-one-url-part
   */
  public function urlsContainingOneUrlPart() {

    $messages = [];
    $host = \Drupal::request()->getHost();

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);

      // Get the node's URL alias.
      // Example: '/foo/bar';
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);

      $uri_segments = explode('/', $alias);

      if (is_array($uri_segments) && count($uri_segments) === 2 ) {
        $messages[] = '<p><a href="https://' . $host . '/' . $uri_segments[1] . '" target="_blank">' . $node->getTitle() . '</a><br>/' . $uri_segments[1] . '</p>';
      }

    }

    $messages_html = '';

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>URLs Containing One URL Part</h2>' . "\n" . $messages_html,
    ];

    return $build;
  }

  /**
   * ESDS-1038: Search and replace Contact Us
   * Route: /admin/config/earthdata/replace-contact-us
   */
  public function replaceContactUs() {

    $messages = [];

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);

      if (!empty($node) && isset($node->field_content)) {
        // Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {
          // Replace the absolute link with nothing.
          if (strstr($paragraph->field_text_content->value, 'javascript:feedback.showForm();')) {

            $paragraph->field_text_content->value = str_replace('javascript:feedback.showForm();', '/contact', $paragraph->field_text_content->value);
            $paragraph->save();

            $messages[] = '<li><a href="https://d3ii0cv1in65ji.cloudfront.net/node/' . $node->id() . '" target="_blank">' . $node->getTitle() . '</a></li>';
          }
        }
      }

    }

    $messages_html = '';
    $info = "\n" . '<p>A report of pages where the Contact Us link has been fixed.</p>' . "\n";

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>Contact Us Fixes Report</h2>' . "\n" . $info . "\n" . '<ol>' . $messages_html . '</ol>' . "\n",
    ];

    return $build;
  }

  /**
   * Find all earthdata subdomains
   * Route: /admin/config/earthdata/earthdata-subdomains
   */
  public function findEarthdataSubdomains() {

    $messages = $subdomains = [];

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);

      if (!empty($node) && isset($node->field_content)) {

        // Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {

          // Search the body for an earthdata.nasa.gov subdomain.
          $regex = '~\w+(?=\.earthdata\.nasa\.gov)~i';
          preg_match_all($regex, $paragraph->field_text_content->value, $matches);

          // Loop through the matches and add them to the $messages array.
          foreach ($matches as $k => $v) {
            foreach ($v as $key => $value) {
              $messages[] = '<li>' . $value . '.earthdata.nasa.gov - <a href="https://d3ii0cv1in65ji.cloudfront.net/node/' . $node->id() . '" target="_blank">' . $node->getTitle() . '</a></li>';
              $subdomains[] = '<li>' . $value . '.earthdata.nasa.gov</li>';
            }
          }

        }
      }

    }

    $messages_html = $subdomains_html = '';
    $info = "\n" . '<p>A report of earthdata.nasa.gov subdomains.</p>' . "\n";

    $subdomains_unique = array_unique($subdomains);
    sort($subdomains_unique);
    foreach ($subdomains_unique as $sk => $sv) {
      $subdomains_html .= "\n" . $sv . "\n";
    }

    sort($messages);
    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>Earthdata Subdomains Report</h2>' . "\n" . $info . "\n" .
         '<h3>Unique Subdomains</h3>' . "\n" . '<ol>' . $subdomains_html . '</ol>' . "\n" . '<hr>' . "\n" .
         '<h3>Subdomains and Locations</h3>' . "\n" . '<ol>' . $messages_html . '</ol>' . "\n",
    ];

    return $build;
  }

  /**
   * Conduit Webinars
   * Route: /admin/config/earthdata/conduit-webinars
   */
  public function conduitWebinars() {

    $result = $rows = [];

    $db_connection = TRUE;

    $header = [
      // ['data' => t('ID'), 'field' => 'id'],
      // ['data' => t('Project ID'), 'field' => 'project_id'],
      ['data' => t('Title'), 'field' => 'title', 'sort' => 'asc'],
      ['data' => t('Type'), 'field' => 'type'],
      ['data' => t('Slug'), 'field' => 'slug'],
      ['data' => t('Author'), 'field' => 'author'],
      // ['data' => t('Ancestry'), 'field' => 'ancestry'],
      ['data' => t('Summary')],
      ['data' => t('Meta Description')],
      ['data' => t('Body')],
      ['data' => t('Science Disciplines')],
      ['data' => t('DAACS')],
      ['data' => t('Other Tags')],
      ['data' => t('GCMD Keywords')],
      ['data' => t('State'), 'field' => 'state'],
      ['data' => t('Is Draft'), 'field' => 'is_draft'],
      ['data' => t('Is Published'), 'field' => 'is_published'],
      // ['data' => t('Featured At'), 'field' => 'featured_at'],
      // ['data' => t('Published At'), 'field' => 'published_at'],
      // ['data' => t('Comment'), 'field' => 'comment'],
      ['data' => t('Created At'), 'field' => 'created_at'],
      ['data' => t('Updated At'), 'field' => 'updated_at'],
      // ['data' => t('Needs Modification'), 'field' => 'needs_modification'],
      ['data' => t('Is Archived'), 'field' => 'is_archived'],
    ];

    // Switch to the Conduit database.
    \Drupal\Core\Database\Database::setActiveConnection('conduit');
    $db = \Drupal\Core\Database\Database::getConnection();

    try {

      // Query Conduit's database.
      $query = $db->select('pages','p')
        ->fields('p')
        ->condition('title', '%Webinar:%', 'LIKE');

      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                    ->orderByHeader($header);
      // Limit the rows to 100 for each page.
      $pager = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender')
                ->limit(100);
      $result = $pager->execute();

    } catch (\Exception $e) {
      // No database
      $rows = [];
      $db_connection = FALSE;
    }

    // Switch back to the default database.
    \Drupal\Core\Database\Database::setActiveConnection();

    if (!empty($result)) {

      foreach ($result as $res) {

        // Decode Conduit's JSON content.
        $json_content = json_decode($res->content);
        // Clean-up empty <p> tags.
        $body_clean = str_replace('<p></p>', '', $json_content->body);

        // Construct the table rows.
        $rows[] = [
          // $res->id,
          // $res->project_id,
          new FormattableMarkup('<strong>@title</strong>',['@title' => $res->title]),
          $res->type,
          $res->slug,
          $res->author,
          // $res->ancestry,
          $json_content->summary,
          $json_content->meta_description,
          $body_clean,
          implode(', ', $json_content->science_disciplines),
          implode(', ', $json_content->daacs),
          implode(', ', $json_content->other_tags),
          implode(', ', $json_content->gcmd_keywords),
          $res->state,
          $res->is_draft,
          $res->is_published,
          // $res->featured_at,
          // $res->published_at,
          // $res->comment,
          $res->created_at,
          $res->updated_at,
          // $res->needs_modification,
          $res->is_archived,
        ];

      }

    }

    $build['table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#attributes'=> ['style' => 'IdforRow'],
      '#sticky' => TRUE,
      '#empty' => !$db_connection ? t('Conduit\'s database is not installed on this server.') : t('No content has been found.'),
    ];

    $build['pager'] = [
      '#type' => 'pager'
    ];

    return $build;
  }

  public function unicode2html($str){
    $i=65535;
    while($i>0){
        $hex=dechex($i);
        $str=str_replace("\u$hex","&#$i;",$str);
        $i--;
     }
     return $str;
  }

  /**
   * Link Scanner
   * Route: /admin/config/earthdata/link-scanner
   */
  public function linkScanner() {

    $messages = [];
    $host = \Drupal::request()->getHost();

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);

      if (!empty($node) && isset($node->field_content) && ($node->status->value === '1')) {

        // Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {

          // Search for: learn/backgrounders/remote-sensors
          if (strstr($paragraph->field_text_content->value, 'learn/backgrounders/remote-sensors')) {
            $messages[] = '<li>learn/backgrounders/remote-sensors - <a href="https://' . $host . $alias . '" target="_blank">' . $node->getTitle() . '</a></li>';
          }

          // Search for: learn/backgrounders/sensors
          if (strstr($paragraph->field_text_content->value, 'learn/backgrounders/sensors')) {
            $messages[] = '<li>learn/backgrounders/sensors - <a href="https://' . $host . $alias . '" target="_blank">' . $node->getTitle() . '</a></li>';
          }

          // Search for: learn/remote-sensors
          if (strstr($paragraph->field_text_content->value, 'learn/remote-sensors')) {
            $messages[] = '<li>learn/remote-sensors - <a href="https://' . $host . $alias . '" target="_blank">' . $node->getTitle() . '</a></li>';
          }

        }
      }

    }

    $messages_html = '';
    $info = "\n" . '<p>A report of pages containing URLs to the Remote Sensors page.</p>' . "\n";
    $info .= "\n" . '<ul>' . "\n";
    $info .= "\n" . '<li>learn/backgrounders/remote-sensors</li>' . "\n";
    $info .= "\n" . '<li>learn/backgrounders/sensors</li>' . "\n";
    $info .= "\n" . '<li>learn/remote-sensors</li>' . "\n";
    $info .= "\n" . '</ul>' . "\n";

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>Link Scanner</h2>' . "\n" . $info . "\n" . '<h3>Results</h3>' . "\n" . '<ol>' . $messages_html . '</ol>' . "\n",
    ];

    return $build;

  }

  /**
   * WST-2613: Change ESDIS Standards Office (ESO) to ESDIS Standards Coordination Office (ESCO) across the site
   * Route: /admin/config/earthdata/replace-eso-esco
   */
  public function replaceEsoEsco() {

    $messages = [];

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);
      // Get the node's URL alias.
      // Example: '/foo/bar';
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);

      if (!empty($node) && isset($node->field_content)) {
        // ESO - Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {
          // Replacements.
          if (strstr($paragraph->field_text_content->value, 'ESO')) {
            // $paragraph->field_text_content->value = str_replace('ESO', 'ESCO', $paragraph->field_text_content->value);
            // $paragraph->save();
            $messages[] = '<li><a href="' . $alias . '" target="_blank">[ESO] ' . $node->getTitle() . '</a></li>';
          }
        }
        // ESCO - Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {
          // Replacements.
          if (strstr($paragraph->field_text_content->value, 'ESDIS Standards Office')) {
            // $paragraph->field_text_content->value = str_replace('ESO', 'ESCO', $paragraph->field_text_content->value);
            // $paragraph->save();
            $messages[] = '<li><a href="' . $alias . '" target="_blank">[ESDIS Standards Office] ' . $node->getTitle() . '</a></li>';
          }
        }
      }

    }

    $messages_html = '';
    $info = "\n" . '<p>A report of pages where "ESDIS Standards Office (ESO)" needs to be changed to "ESDIS Standards Coordination Office (ESCO)".</p>' . "\n";
    $info .= "\n" . '<p>This report is divided between occurrences of "ESO" and "ESDIS Standards Office" in case the acronym and full name exist independently.</p>' . "\n";

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => $info . "\n" . '<ol>' . $messages_html . '</ol>' . "\n",
    ];

    return $build;
  }

  /**
   * WST-2610: Change On Board to Aboard programmatically
   * Route: /admin/config/earthdata/replace-on-board-aboard
   */
  public function replaceOnBoardAboard() {

    $messages = [];

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);
      // Get the node's URL alias.
      // Example: '/foo/bar';
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);

      if (!empty($node) && isset($node->field_content)) {
        // On Board - Loop through the field_content field's referenced paragraphs entities.
        foreach ($node->field_content->referencedEntities() as $paragraph) {
          // Replacements.
          if (strstr($paragraph->field_text_content->value, 'on board')) {
            // $paragraph->field_text_content->value = str_replace('on board', 'aboard', $paragraph->field_text_content->value);
            // $paragraph->save();
            $messages[] = '<li><a href="' . $alias . '" target="_blank">' . $node->getTitle() . '</a></li>';
          }
        }
      }

    }

    $messages_html = '';
    $info = "\n" . '<p>A report of pages where "on board" needs to be changed to "aboard".</p>' . "\n";

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => $info . "\n" . '<ol>' . $messages_html . '</ol>' . "\n",
    ];

    return $build;
  }

  /**
   * Bulk Update the Authored By Field Via URL Path
   * Route: /admin/config/earthdata/bulk-update-authored-by-via-url-path
   * Note: 88 = rebagwel
   */
  public function bulkUpdateAuthoredByViaUrlPath() {

    // Set the target UID and username.
    $uid = '88';
    $username = 'rebagwel';

    // Reset the target UID if the $_GET['uid'] value exists.
    if (array_key_exists('uid', $_GET)) {
      $uid = filter_input(INPUT_GET, 'uid', FILTER_SANITIZE_STRING);
    }
    // Reset the target username if the $_GET['username'] value exists.
    if (array_key_exists('username', $_GET)) {
      $username = filter_input(INPUT_GET, 'username', FILTER_SANITIZE_STRING);
    }

    $messages = [];
    $host = \Drupal::request()->getHost();

    // Get all node ids.
    $ids = \Drupal::entityQuery('node')->execute();

    $i = 0;

    foreach ($ids as $key => $nid) {

      // Load the node entity.
      $node = \Drupal\node\Entity\Node::load($nid);

      // Get the node's URL alias.
      // Example: '/foo/bar';
      $alias = \Drupal::service('path_alias.manager')->getAliasByPath('/node/' . $nid);

      $uri_segments = explode('/', $alias);

      if ( $uri_segments[1] === 'esdis' || $uri_segments[1] === 'eosdis'  || (array_key_exists(2, $uri_segments) && $uri_segments[2] === 'sensing-our-planet') ) {

        // Change the Authored by UID.
        if ($node->getOwnerID() !== $uid) {

          $node->set('uid', $uid);
          $node->save();
          $messages[] = '<p>' . ($i+1) . '. <a href="https://' . $host . $alias . '" target="_blank">' . $node->getTitle() . '</a><br>Path: ' .$alias . '</p>';
          $i++;

        }

      }

    }

    $messages_html = '';

    if (!empty($messages)) {
      $messages_html .= "\n" . '<p>The "Authored by" field has been changed to UID ' . $uid . ' (' . $username . ') for ' . $i . ' nodes with URL paths which start with /esdis, /eosdis, and /learn/sensing-our-planet.</p>' . "\n";
    }

    foreach ($messages as $mk => $mv) {
      $messages_html .= "\n" . $mv . "\n";
    }

    if (empty($messages_html)) {
      $messages_html .= "\n" . '<li>No messages were generated.</li>' . "\n";
    }

    $build['content'] = [
      '#markup' => '<h2>Changed the "Authored by" field value to UID ' . $uid . ' (' . $username . ') for ' . $i . ' nodes</h2>' . "\n" . $messages_html,
    ];

    return $build;
  }

}
