/**
 * @file
 * Provides JavaScript additions to ckeditor tabs bootstrap.
 *
 * This file provides bootstrap tabs in ckeditor.
 */

(function ($, Drupal) {

  "use strict";

  var initCKEditorTabs = function() {
    for (var i in CKEDITOR.instances) {
      if (CKEDITOR.instances[i].document) {
        var $tabComponents = $(CKEDITOR.instances[i].document.$.body).find('.bootstrap-tabs');
        tabsInitAdmin($tabComponents);
      } else {
        CKEDITOR.instances[i].on('instanceReady', function (instanceReadyEventObj) {
          var editorInstanceData = CKEDITOR.instances[i].document.getBody();
          var $tabComponents = $(editorInstanceData.$).find('.bootstrap-tabs');
          tabsInitAdmin($tabComponents);
        });
      }
    }
  };

  /**
   * Attach behaviors to tabs for ckeditor.
   */
  Drupal.behaviors.ckeditorTabs = {
    attach: function (context, settings) {
      if (typeof CKEDITOR === 'undefined') {
        var $viewTabs = $('.earthdata-tabs', context);
        if ($viewTabs.length > 0) {
          tabsInit($viewTabs);
        }
        return;
      }

      initCKEditorTabs();

      CKEDITOR.on("instanceReady", function() {
        initCKEditorTabs();
      });
    }
  };

  function tabsInitAdmin(elements) {

    var $tabComponents = elements;

    if ($tabComponents.length > 0) {
      $tabComponents.each(function () {
        var $tabs = $(this).find('.nav-tabs');
        $tabs
          .off('click', 'li > a')
          .on('click', 'li > a', function (e) {
            e.preventDefault();

            var link = $(this);

            link
              .parent().addClass('active')
              .siblings().removeClass('active');

            link.parents('.bootstrap-tabs').find(link.attr('href')).addClass('active')
              .siblings().removeClass('active');
          })
      });
    }
  }

  function tabsInit(elements) {

    var $tabComponents = elements;

    if ($tabComponents.length > 0) {

      /*** Activate Bootstrap tab based on the URL hash. ***/
      /*** Example: foo/bar#tab-12345-1 ***/
      var hash = document.location.hash;

      if (hash) {

        // Prevent anchor "jump" on page load.
        setTimeout(function() {

          // If the browser is Firefox.
          if(navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
            window.scrollTo(0, 0);
          } else {
            // All other browsers.
            var x = window.pageXOffset,
                y = window.pageYOffset;

            $(window).one('scroll', function () {
              window.scrollTo(x, y);
            });
          }

        }, 1);


        var levelOne = $('#tabs-level-one li a'),
            levelTwo = $('.bootstrap-tabs .earthdata-tabs');

        // Deactivate all parent tabs.
        levelOne.each(function( index ) {
          $( this ).removeClass('active')
        })
        // Activate the target parent tab.
        $('.nav-tabs a[href=\\' + hash + ']').addClass('active');
        // Deactivate all child tabs.
        $('#tabs-content').find('.tab-pane').removeClass('active');
        // Activate the target child tab.
        $(hash).addClass('active');
        $(hash).find('.earthdata-tabs li:first-child').addClass('active');
        $(hash).find('.earthdata-tabs li:first-child a').addClass('active');
        // Activate the tab content.
        var tabContentId = $(hash).find('.earthdata-tabs li:first-child a').attr('href');
        $(tabContentId).addClass('active');
      }

      /*** Change the URL hash when a Bootstrap tab is activated. ***/
      $('a[data-bs-toggle="tab"]').on('show.bs.tab', function (e) {
        // Set the URL hash.
        window.location.hash = e.target.hash
        // Highlight the first tabs on the second tier.
        $('.bootstrap-tabs .earthdata-tabs').each(function( index ) {
          // Set variables.
          var thisLevelTwoTabGroup = $(this),
              thisLevelTwoFirstTab = thisLevelTwoTabGroup.find('li a').first();
          // Remove all "active" classes
          thisLevelTwoTabGroup.find('li a').removeClass('active');
          thisLevelTwoTabGroup.find('li').removeClass('active');
          // Add "active" classes to the targets.
          thisLevelTwoFirstTab.addClass('active');
          thisLevelTwoFirstTab.parent().addClass('active');
        });
      })

      /*** Click events. ***/
      $tabComponents.each(function () {

        var tabsLevelOne = $('.earthdata-tabs');
        var tabsLevelTwo = $('.bootstrap-tabs .earthdata-tabs');

        // Hack to remove the 'active' CSS class from <li> elements.
        tabsLevelOne.find('li').removeClass('active');

        // Highlight the first tab.
        tabsLevelTwo.each(function( index ) {
          // Set variables.
          var thisTabGroup = $(this),
              thisFirstTab = thisTabGroup.find('li a').first();
          // Remove all "active" classes
          thisTabGroup.find('li a').removeClass('active');
          thisTabGroup.find('li').removeClass('active');
          // Add "active" classes to the targets.
          thisFirstTab.addClass('active');
          thisFirstTab.parent().addClass('active');
        });

        tabsLevelOne
          .off('click', 'a')
          .on('click', 'a', function (e) {
            e.preventDefault();

            var link = $(this);
            var id = link.attr('href');
            var tabContent = $('.bootstrap-tabs .tab-content .tab-pane');
            var firstLevelTabs = $('#tabs-level-one');

            // Toggle tabs.
            link.parent().parent().find('li.nav-item').removeClass('active');
            link.parent().parent().find('a.nav-link').removeClass('active');
            link.parent().addClass('active');
            link.addClass('active');

            // Deactivate all of the first level tab panes.
            tabContent.each(function( index ) {
              $(this).removeClass('active');
            });
            // Activate the first level tab pane.
            $(id).addClass('active');

            // Activate the second level tab pane.
            firstLevelTabs.each(function( index ) {
              // Set variables.
              var thisTabGroup = $(this),
                  secondLevelTabs = $('.bootstrap-tabs .earthdata-tabs');
              // Activate the first tab pane.
              secondLevelTabs.each(function( index ) {
                var first = $(this).find('li.active a').first(),
                    secondId = first.attr('href');
                $(secondId).addClass('active');
              });

            });

          });
      });
    }
  }

})(jQuery, Drupal);
