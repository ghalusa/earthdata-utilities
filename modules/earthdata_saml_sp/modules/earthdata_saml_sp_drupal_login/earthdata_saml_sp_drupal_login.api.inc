<?php

/**
 * @file
 * API hooks for SAML Service Provider Drupal Login module.
 */


/**
 * allow the user to be altered based on attributes passed in from the SAML IdP
 *
 * @param \Drupal\user\UserInterface $user
 * @param array $attributes
 *
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function hook_earthdata_saml_sp_drupal_login_user_attributes(Drupal\user\UserInterface $user, array $attributes) {
  // do something with the user account

  // make sure you save the $user object or the changes won't stick
  $user->save();
}
